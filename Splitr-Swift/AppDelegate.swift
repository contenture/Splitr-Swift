//
//  AppDelegate.swift
//  Splitr-Swift
//
//  Created by Patrick van Zadel on 24/02/15.
//  Copyright (c) 2015 Patrick van Zadel. All rights reserved.
//

import Cocoa
import AppKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }

}

