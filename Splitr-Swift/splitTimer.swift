//
//  timer.swift
//  Splitr-Swift
//
//  Created by Patrick van Zadel on 24/02/15.
//  Copyright (c) 2015 Patrick van Zadel. All rights reserved.
//
import Cocoa
import Foundation

public class splitTimer: NSObject {
    
    var startTime = NSTimeInterval()
    var splitTextLabel: NSTextField?
    
    override init() {
        super.init()
        
        println("Hello World")
    }
    
    func updateTime(timerLabel: NSTextField) {
        var currentTime = NSDate.timeIntervalSinceReferenceDate()
        var elapsedTime: NSTimeInterval = currentTime - startTime
        
        let hours = UInt8(elapsedTime / 3600.0)
        elapsedTime -= (NSTimeInterval(hours) * 3600)
        
        let minutes = UInt8(elapsedTime / 60.0)
        elapsedTime -= (NSTimeInterval(minutes) * 60)
        
        let seconds = UInt8(elapsedTime)
        elapsedTime -= NSTimeInterval(seconds)
        
        let fraction = UInt8(elapsedTime * 100)
        
        let strHours = hours > 9 ? String(hours):"0" + String(hours)
        let strMinutes = minutes > 9 ? String(minutes):"0" + String(minutes)
        let strSeconds = seconds > 9 ? String(seconds):"0" + String(seconds)
        let strFraction = fraction > 9 ? String(fraction):"0" + String(fraction)
        
        timerLabel.stringValue = "\(strHours):\(strMinutes):\(strSeconds).\(strFraction)"
    }
    
    var timer = NSTimer()
    
    func start() {
        let updateSelector: Selector = "updateTime"
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: updateSelector, userInfo: nil, repeats: true)
        startTime = NSDate.timeIntervalSinceReferenceDate()
    }
    
}