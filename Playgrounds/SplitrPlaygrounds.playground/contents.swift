// Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"
var splitrText: NSTextField?

public class splitTimer {
    
    private let timer: NSTextField
    private var startTime = NSTimeInterval()
    private var split = NSTimer()
    
    init(timer: NSTextField) {
        self.timer = timer
    }
    
    public func start() {
        let updateSelector: Selector = "updateTimer"
        split = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: updateSelector, userInfo: nil, repeats: true)
        
        startTime = NSDate.timeIntervalSinceReferenceDate()
    }
    
    func updateTimer(timer: NSTextField) {
        var currentTime = NSDate.timeIntervalSinceReferenceDate()
        var elapsedTime: NSTimeInterval = currentTime - startTime
        
        let hours = UInt8(elapsedTime / 3600.0)
        elapsedTime -= NSTimeInterval(hours) * 3600
        
        let minutes = UInt8(elapsedTime / 60.0)
        elapsedTime -= (NSTimeInterval(minutes) * 60)
        
        let seconds = UInt8(elapsedTime)
        elapsedTime -= NSTimeInterval(seconds)
        
        let fraction = UInt8(elapsedTime * 100)
        
        let strHours = hours > 9 ? String(hours):"0" + String(hours)
        let strMinutes = minutes > 9 ? String(minutes):"0" + String(minutes)
        let strSeconds = seconds > 9 ? String(seconds):"0" + String(seconds)
        let strFraction = fraction > 9 ? String(fraction):"0" + String(fraction)
        
        timer.stringValue = "\(strHours):\(strMinutes):\(strSeconds).\(strFraction)"
    }
}

var splitr = splitTimer(timer: splitrText!)